<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Thank You</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <script src="main.js"></script> -->
    <link href="<c:url value="/resources/style.css" />" rel="stylesheet">
    <script type="text/javascript"  src="<c:url value="/resources/main.js" />"></script>
</head>
<body>
        <div class="rectangle"></div>
        <div class="shape"></div>
        <!-- <div class="thankYou"></div> -->
        <div class="thankYourectangle">
            <img  src="<c:url value="resources/digit-mobile.svg" />" alt="" class="digitImage">
            <img src="<c:url value="resources/thank-you-banner-with-decorative-flora-mobile.svg" />"alt="" class="thankYou">
            <p class="we-appriciate-your-v">We appriciate your valuable time for giving us feedback.</p>
            <p class="we-appriciate-your-v">This will help us serve you better in future.</p>
        </div>
</body>
</html>