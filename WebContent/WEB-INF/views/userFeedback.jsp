<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>User Feedback</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<c:url value="/resources/style.css" />" rel="stylesheet">
    <script type="text/javascript"  src="<c:url value="/resources/main.js" />"></script>
</head>
<body>
    <div class="mainContent">
        <div class="rectangle"></div>
        <div class="firework"></div>
        <div class="starRectangle">
            <img src="<c:url value="resources/digit.png" />" alt="" class="digitImage"/>
            <div class="how-was-your-overall">How was your overall experience?</div>
            <div>
                    <span><img alt=""  src="<c:url value="resources/whiteStar.svg" />"  class="start1" id="star1" onclick="colorStar(1)"/></span>
                    <span><img alt=""  src="<c:url value="resources/yellowStar.svg" />"  alt="" class="starYellow1" id="starYellow1" onclick="unColorStar(1)" style="display: none"/></span>
                    <span><img alt=""  src="<c:url value="resources/whiteStar.svg" />"alt="" class="start2" id="star2" onclick="colorStar(2)"/></span>
                    <span><img alt=""  src="<c:url value="resources/yellowStar.svg" />" alt="" class="starYellow2" id="starYellow2" onclick="unColorStar(2)" style="display: none"/></span>
                    <span><img alt=""  src="<c:url value="resources/whiteStar.svg" />" alt="" class="start3" id="star3" onclick="colorStar(3)"/></span>
                    <span><img alt=""  src="<c:url value="resources/yellowStar.svg" />"alt="" class="starYellow3" id="starYellow3" onclick="unColorStar(3)" style="display: none"/></span>
                    <span><img alt=""  src="<c:url value="resources/whiteStar.svg" />"alt="" class="start4" id="star4" onclick="colorStar(4)"/></span>
                    <span><img alt=""  src="<c:url value="resources/yellowStar.svg" />"alt="" class="starYellow4" id="starYellow4" onclick="unColorStar(4)" style="display: none"/></span>
                    <span><img alt=""  src="<c:url value="resources/whiteStar.svg" />"alt="" class="start5" id="star5" onclick="colorStar(5)"/></span>
                    <span><img alt=""  src="<c:url value="resources/yellowStar.svg" />"alt="" class="starYellow5" id="starYellow5" onclick="unColorStar(5)" style="display: none"/></span>
            </div>
        </div>
        <form action="feedBack" method="post">
        <div class="feedbackrectangle">
            <div class="tell-us-what-you-don">Tell us what you do not like</div>
            <div>
                <textarea name="userFeedback" id="userFeedback" class="textarea" placeholder="You can talk about our process, turn around time or anything that you feel can be improved.."></textarea>
                <input type="text"  name="starRating" id="starRating" style="display: none">
                <input type="text"  name="feedbacksrc" id="feedbacksrc" value='${feedbacksrc}' style="display: none">
                <input type="text"  name="feedbackey" id="feedbackey" value='${feedbackKey}' style="display: none">
            </div>
        </div>
        <div style="width:100%">
            <input type="submit" style="text-align: center" value="Send" class="button"/>
        </div>
        </form>
    </div>
</body>
</html>