<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Submitted</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<c:url value="/resources/style.css" />" rel="stylesheet">
</head>
<body>
        <div class="rectangle"></div>
        <div class="shape"></div>
        <div class="thankYourectangle">
        		<img  src="resources/digit-mobile.svg" alt="" class="digitImage">
                <img  src="<c:url value="resources/cloud-customer-care-mobile.svg" />"alt="" class="submitted">
                <p class="thanks-for-coming-ba">Thanks for coming back!</p>
                <p class="youve-already-submi">You've already submitted the feedback.</p>
        </div>
</body>
</html>