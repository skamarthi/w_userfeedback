package com.godigit.model;

public class FeedBackModel {
	
	private String starRating;
	
	private String feedbacksrc;
	
	private String feedbackey;
	
	private String userFeedback;

	public String getStarRating() {
		return starRating;
	}

	public void setStarRating(String starRating) {
		this.starRating = starRating;
	}

	public String getFeedbacksrc() {
		return feedbacksrc;
	}

	public void setFeedbacksrc(String feedbacksrc) {
		this.feedbacksrc = feedbacksrc;
	}

	public String getFeedbackey() {
		return feedbackey;
	}

	public void setFeedbackey(String feedbackey) {
		this.feedbackey = feedbackey;
	}

	public String getUserFeedback() {
		return userFeedback;
	}

	public void setUserFeedback(String userFeedback) {
		this.userFeedback = userFeedback;
	}

}
