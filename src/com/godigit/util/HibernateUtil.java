package com.godigit.util;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

	private static Object obj = new Object();		
	private static SessionFactory sessionFactory;
	private static SessionFactory readOnlySessionFactory;

	private static StandardServiceRegistry registry;
	static Logger logger = Logger.getLogger(HibernateUtil.class.getName());

	@SuppressWarnings("deprecation")
	public static SessionFactory buildSessionFactory() {

		logger.info("buildSessionFactory called..!");

		if (sessionFactory == null) {

			synchronized (obj) {

				if (sessionFactory == null) {
					try {

						
						  /*StandardServiceRegistry standardRegistry = new
						  StandardServiceRegistryBuilder() .configure("hibernate.cfg.xml").build();
						  Metadata metaData = new MetadataSources(
						  standardRegistry).getMetadataBuilder().build(); sessionFactory =
						  metaData.getSessionFactoryBuilder() .build();*/
						 
						Configuration configuration = new Configuration();
						configuration.configure("hibernate.cfg.xml");
						sessionFactory = configuration.buildSessionFactory();
					} catch (Throwable ex) {
						logger.error("buildSessionFactory", ex);
					}
				}
			}

		}

		return sessionFactory;
	}

	/**
	 * @return
	 */
	public static SessionFactory getSessionFactory() {

		logger.info("SessionFactory called");

		if (sessionFactory == null) {
			buildSessionFactory();
		}

		return sessionFactory;
	}

	/**
	 * used for creating another Sessionfactory connected to replica server for GET
	 * calls in InsuranceActivity
	 */
	public static SessionFactory buildReadOnlySessionFactory() {
		logger.info("buildSessionFactory 2 called..!");
		if (readOnlySessionFactory == null) {
			synchronized (obj) {
				if (readOnlySessionFactory == null) {
					try {
						Configuration configuration = new Configuration();
						configuration.configure("hibernate-replica.cfg.xml");
						readOnlySessionFactory = configuration.buildSessionFactory();
					} catch (Throwable ex) {
						logger.error("buildSessionFactory2", ex);
					}
				}
			}
		}
		return readOnlySessionFactory;
	}

	/**
	 * @return Session Factory associated with Replica DB
	 */
	public static SessionFactory getReadOnlySessionFactory() {

		logger.info("getReadOnlySessionFactory called");

		if (readOnlySessionFactory == null) {
			buildReadOnlySessionFactory();
		}

		return readOnlySessionFactory;
	}

	/**
	 * 
	 */
	public static void shutdownSessions() {

		logger.info("shutdownSessions called");

		try {
			if (sessionFactory != null) {
				sessionFactory.close();
			}
			if (registry != null) {
				StandardServiceRegistryBuilder.destroy(registry);
				Enumeration<Driver> drivers = DriverManager.getDrivers();
				while (drivers.hasMoreElements()) {
					Driver driver = drivers.nextElement();
					try {
						DriverManager.deregisterDriver(driver);
						logger.info("De-regsitering driver in application context class");
					} catch (SQLException e) {
						logger.error("SQL Exception in shutdown sessions", e);
					}

				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

		}

	}

}
