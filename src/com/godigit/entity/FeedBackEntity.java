package com.godigit.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.gson.annotations.Expose;

@Entity
@Table(name = "t_user_feedback", schema = "digit_activity")
public class FeedBackEntity {
	
		@Id
		@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "digit_activity.t_user_feedback_id_seq")
		@SequenceGenerator(name = "digit_activity.t_user_feedback_id_seq", sequenceName = "digit_activity.t_user_feedback_id_seq", allocationSize = 1)
		@Column(name = "feedback_id", nullable = false)
		@Expose
		private Long feedbackId;
		
		@Column(name = "feedback_source")
		@Expose
		private String feedbackSource;
		
		@Column(name = "stars")
		@Expose
		private Integer stars;
		
		@Column(name = "remark")
		@Expose
		private String description;
		
		@Column(name = "feedback_key", unique = true)
		@Expose
		private String feedbackKey;
		
		@CreationTimestamp
	    @Column(name = "created_date")
	    @Temporal(TemporalType.TIMESTAMP)
	    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MMM-dd HH:mm:ss")
	    private Date createdDate;
		
		@Column(name = "feedback_activity", unique = true)
		@Expose
		private String feedbackActivity;
	
		public Long getFeedbackId() {
			return feedbackId;
		}
	
		public void setFeedbackId(Long feedbackId) {
			this.feedbackId = feedbackId;
		}
	
		public String getFeedbackSource() {
			return feedbackSource;
		}
	
		public void setFeedbackSource(String feedbackSource) {
			this.feedbackSource = feedbackSource;
		}
	
		public Integer getStars() {
			return stars;
		}
	
		public void setStars(Integer stars) {
			this.stars = stars;
		}
	
		public String getDescription() {
			return description;
		}
	
		public void setDescription(String description) {
			this.description = description;
		}
	
		public String getFeedbackKey() {
			return feedbackKey;
		}
	
		public void setFeedbackKey(String feedbackKey) {
			this.feedbackKey = feedbackKey;
		}
	
		public Date getCreatedDate() {
			return createdDate;
		}
	
		public void setCreatedDate(Date createdDate) {
			this.createdDate = createdDate;
		}

		public String getFeedbackActivity() {
			return feedbackActivity;
		}

		public void setFeedbackActivity(String feedbackActivity) {
			this.feedbackActivity = feedbackActivity;
		}

}
