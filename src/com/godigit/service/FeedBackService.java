package com.godigit.service;

import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Component;

import com.godigit.entity.FeedBackEntity;
import com.godigit.util.HibernateUtil;

@Component
public class FeedBackService {
	
	Logger logger = Logger.getLogger(FeedBackService.class.getName());

	public void newFeedBack(FeedBackEntity FeedBackEntity) {
		try {
			Session session = null;
			Transaction transaction = null;
			SessionFactory sessionfactory = HibernateUtil.getSessionFactory();
			String sessionId = UUID.randomUUID().toString();
			session = sessionfactory.openSession();
			logger.info("session opened : " + sessionId);
			transaction = session.getTransaction();
			transaction.begin();
			session.save(FeedBackEntity);
			session.flush();
			session.clear();
			transaction.commit();
		} catch (ConstraintViolationException e) {
			logger.error("constraint violated while trying to insert  :" + FeedBackEntity.getFeedbackKey());
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<FeedBackEntity> getFeedBack(String key,String activity) {
		Session session = null;
		Transaction transaction = null;
		SessionFactory sessionfactory = HibernateUtil.getSessionFactory();
		String sessionId = UUID.randomUUID().toString();
		session = sessionfactory.openSession();
		logger.info("session opened : " + sessionId);
		transaction = session.getTransaction();
		transaction.begin();
		Query query = session.createQuery(" from FeedBackEntity fde where fde.feedbackKey = :fkey and fde.feedbackActivity = :factivity");
		query.setParameter("fkey", key);
		query.setParameter("factivity", activity);
		List<FeedBackEntity> feedBacks = query.list();
		session.flush();
		session.clear();
		transaction.commit();
		return feedBacks;
	}

	public void deleteFeedBack(Long id) {
		try {
			Session session = null;
			Transaction transaction = null;
			SessionFactory sessionfactory = HibernateUtil.getSessionFactory();
			String sessionId = UUID.randomUUID().toString();
			session = sessionfactory.openSession();
			logger.info("session opened : " + sessionId);
			transaction = session.getTransaction();
			transaction.begin();
			FeedBackEntity deleteFeedBack = session.load(FeedBackEntity.class, id);
			session.delete(deleteFeedBack);
			session.flush();
			session.clear();
			transaction.commit();
		} catch (ObjectNotFoundException e) {
			logger.error("record with id : " + id + " not found to delete");
			throw e;
		}
	}

}
