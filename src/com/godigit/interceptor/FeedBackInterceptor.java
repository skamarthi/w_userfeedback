package com.godigit.interceptor;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class FeedBackInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		// Get full URL (http://user:pwd@www.example.com/root/some?k=v#hey)
		URI requestUri = new URI(request.getRequestURL().toString());
		// and strip last parts (http://user:pwd@www.example.com/root)
		URI contextUri = new URI(requestUri.getScheme(), requestUri.getAuthority(), request.getContextPath(), null,
				null);
		String[] url = requestUri.toString().split("/");
		String number = url[url.length-1];
		request.getParameterNames();

		for (String baseurl : url) {
			if (baseurl.equals("claimNumber")) {
				response.sendRedirect("UserFeedBack/feedback/"+number);
				return false;
			} else if (baseurl.equals("policyNumber")) {
				response.sendRedirect(request.getContextPath()+"/user.jsp");
				return false;
			}
		}

		return true;
	}
}
