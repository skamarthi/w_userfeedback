package com.godigit.userFeedback.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.godigit.entity.FeedBackEntity;
import com.godigit.service.FeedBackService;

@Controller
@RequestMapping("/")
public class FeedBackController {

	Logger logger = Logger.getLogger(FeedBackController.class.getName());

	@Autowired
	FeedBackService feedbackService;

	@RequestMapping(value = "/new", method = RequestMethod.POST)
	public ResponseEntity<Void> createUser(@RequestBody FeedBackEntity feedbackEntity, UriComponentsBuilder ucBuilder) {
		System.out.println("Creating User " + feedbackEntity.getFeedbackSource());
		feedbackService.newFeedBack(feedbackEntity);

		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<FeedBackEntity> deleteFeedBack(@PathVariable("id") long id) {
		logger.info("service to delete feedback with feedbackid:" + id);

		try {
			feedbackService.deleteFeedBack(id);
			return new ResponseEntity<FeedBackEntity>(HttpStatus.NO_CONTENT);
		} catch (ObjectNotFoundException e) {
			return new ResponseEntity<FeedBackEntity>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/claimNumber/{claim}", method = RequestMethod.GET)
	public String getUserFeedBack(@PathVariable("claim") String claim) {
		System.out.println("Inside  getUserFeedBack ");
		try {
			// feedbackService.newFeedBack(feedbackEntity);
		} catch (Exception e) {
			return "error";
		}
		return "thankYou";
	}

	@RequestMapping(value = "/policyNumber/{policy}", method = RequestMethod.GET)
	public String getFeedBack(@PathVariable("policy") String policy) {
		System.out.println("Inside  getFeedBack ");
		try {
			// feedbackService.newFeedBack(feedbackEntity);
		} catch (Exception e) {
			return "error";
		}
		return "thankYou";
	}
}
