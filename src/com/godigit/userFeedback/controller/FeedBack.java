package com.godigit.userFeedback.controller;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.godigit.entity.FeedBackEntity;
import com.godigit.model.FeedBackModel;
import com.godigit.model.User;
import com.godigit.service.FeedBackService;

@Controller
@RequestMapping("/")
public class FeedBack {
	
	Logger logger = Logger.getLogger(FeedBackController.class.getName());
	
	@Autowired
	FeedBackService feedBackService; 
	
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public String user(@Validated User user, Model model) {
		model.addAttribute("userName", user.getUserName());
		return "user";
	}
	
	@RequestMapping(value = "/feedBack", method = RequestMethod.POST)
	public String user(@Validated FeedBackModel feedBack, Model model) {
		model.addAttribute("starRating", feedBack.getStarRating());
		FeedBackEntity feedBackEntity = new FeedBackEntity();
		feedBackEntity.setFeedbackKey(feedBack.getFeedbackey());
		feedBackEntity.setFeedbackSource(feedBack.getFeedbacksrc());
		feedBackEntity.setDescription(feedBack.getUserFeedback());
		feedBackEntity.setStars(Integer.valueOf(feedBack.getStarRating()));
		feedBackEntity.setCreatedDate(new Date());
		feedBackEntity.setFeedbackActivity(feedBack.getFeedbackey()+feedBack.getFeedbacksrc());
		try {
			feedBackService.newFeedBack(feedBackEntity);
		}catch(Exception e) {
			return "error";
		}
		return "thankYou";
	}
	
	@RequestMapping(value = "/feedback", method = RequestMethod.GET)
	public String getAnythingelse(@RequestParam(value = "feedbackSource", required = false) String feedbackSource,
			@RequestParam(value = "feedbackKey", required = false) String feedbackKey, Model model) {
		model.addAttribute("feedbacksrc", feedbackSource);
		model.addAttribute("feedbackKey", feedbackKey);

		String feedBackActivity = feedbackKey + feedbackSource;
		// checking already submitted the feedback or not
		logger.info("service to get feegback for key:" + feedbackKey + " and activty: " + feedBackActivity + " called");
		try {
			List<FeedBackEntity> feedbackEntity = feedBackService.getFeedBack(feedbackKey, feedBackActivity);
			if (feedbackEntity == null || feedbackEntity.isEmpty()) {
				logger.info("FeedBackEntity with key " + feedbackKey + " and activity : " + feedBackActivity
						+ " not found");
				return "userFeedback";
			}
		} catch (Exception e) {
			return "error";
		}
		return "submitted";
	}
}
